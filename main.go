package main

import (
	"context"
	"github.com/aws/aws-lambda-go/lambda"
	"log"
)

type event struct {
	Name string `json:"name"`
}

func main() {
	log.SetFlags(log.Lshortfile | log.LstdFlags | log.Lmicroseconds)

	lambda.Start(func(ctx context.Context, eve event) error {
		log.Printf("こんにちわーるどよろしくね: %s\n", eve.Name)
		return nil
	})
}
