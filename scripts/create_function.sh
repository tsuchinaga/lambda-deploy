aws lambda create-function \
    --function-name TestFunction \
    --runtime go1.x \
    --zip-file fileb://function.zip \
    --handler main \
    --role arn:aws:iam::352548536038:role/0_LambdaBasicExecutionRole
