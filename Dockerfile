FROM golang:1.13

### install pip -> install awscli
RUN curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py" \
    && python get-pip.py \
    && rm get-pip.py \
    && pip install awscli

### install zip
RUN apt update -y && apt install -y zip
